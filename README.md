# Demonstração

![demo.gif](demo.gif)

# Testando o Chatbot

É necessário estar definido como desenvolvedor do aplicativo [Ada (Facebook for Developers)](https://developers.facebook.com/apps/249931432859447).
O aplicativo está em análise pela equipe do Facebook, e nesse estado o chatbot só funciona para desenvolvedores.

Abra uma conversa com a Ada no Messenger: [http://m.me/chamaada](http://fb.me/chamaada)

Para receber dicas sobre finanças, envie um "Oi", "Olá Ada" ou algo similar.
Ela responderá com uma dica, um lembrete ou algum alerta.

Para se cadastrar no sistema, envie "Quero me cadastrar".
Em seguida, responda às perguntas feitas pela Ada.
Ela incluirá seus dados no banco de dados.

Para visualizar os dados existentes no banco de dados,
[acesse a API](http://hackathon-sebrae.duckdns.org/pessoas)

Outra maneira de visualizar os dados é através [desta interface web](http://hackathon-sebrae.duckdns.org:8080/)
utilizando as seguintes credenciais:

* Usuário: `lacasa`
* Senha: `lacasa123`
* Base de dados: `lacasa`

# Arquitetura de Software

![software-architecture.png](software-architecture.png)

O sistema é composto por:

### REST API

* Permite o gerenciamento dos cadastros das pessoas que utilizam o sistema
* URL: [http://hackathon-sebrae.duckdns.org/pessoas](http://hackathon-sebrae.duckdns.org/pessoas)
* Código fonte: [https://gitlab.com/la-casa-de-papel/rest-api](https://gitlab.com/la-casa-de-papel/rest-api)
* Construída utilizando a linguagem Java e o framework Spring
* Containerizada com Docker
* A construção da imagem Docker acontece de forma automática através do [GitLab CI/CD](https://gitlab.com/la-casa-de-papel/rest-api/pipelines)
* A imagem construída é armazenada [neste registry](https://gitlab.com/la-casa-de-papel/rest-api/container_registry)

### Swarm Cluster

* Responsável por orquestrar os containers da API
* Código fonte: [https://gitlab.com/la-casa-de-papel/swarm-cluster](https://gitlab.com/la-casa-de-papel/swarm-cluster)
* Os recursos de infraestrutura para formar o cluster são construídos utilizando Terraform
* A configuração do cluster é feita utilizando Ansible

### AWS Lex Chatbot

* Responsável por interagir com o usuário atraves de mensagens
* É integrado ao Messenger do Facebook

### AWS Lambda Function

* Responsável por receber as informações que o chatbot obteve da pessoa e enviá-las para a API
* O código da função é escrito na linguagem Node.js

# Tecnologias Usadas

![technologies.png](technologies.png)

# Criando a Infraestrutura na AWS (utilizando Terraform)

Crie um bucket no Amazon S3:

* Região: `us-east-1`
* Nome: `ada-baiana`
* Bloquear todo o acesso público

Crie um usuário no Amazon IAM:

* Nome: `ada-baiana`
* Tipo de Acesso: **Acesso Programático**
* Adicionar Permissões: **AmazonS3FullAccess** e **AmazonEC2FullAccess**
* Copie o access-key-id e o secret-access-key e guarde em um local seguro

Crie um key-pair no Amazon EC2:

* Nome: `lacasa`
* Faça o download do arquivo em: `~/Downloads/lacasa.pem`
* Execute o comando `chmod 400 ~/Downloads/lacasa.pem`

Clone o projeto `swarm-cluster`:

```shell
git clone https://gitlab.com/la-casa-de-papel/swarm-cluster.git
cd swarm-cluster
```

Execute um container Docker que contém o Terraform:

```shell
docker run -it --rm -v $(pwd)/terraform:/app -w /app --entrypoint "" \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \
  hashicorp/terraform sh
```

Inicialize o Terraform:

```shell
terraform init
```

Crie os recursos de infraestrutura:

```shell
terraform plan -out tfplan
terraform apply tfplan
```

Copie o IP público do cluster:

```shell
terraform output public_ip
```

Configure o DNS:

* [Duck DNS](https://www.duckdns.org/)

Crie os arquivos com os IPs das instâncias EC2:

```shell
terraform output manager_hosts > manager_hosts
terraform output worker_hosts  > worker_hosts
```

Encerre o container:

```shell
exit
```

# Configurando o Cluster Swarm (utilizando Ansible)

Prepare o inventário e o playbook do Ansible:

```shell
echo "[docker_swarm_manager]" > ansible/hosts
cat terraform/manager_hosts >> ansible/hosts

echo "[docker_swarm_worker]" >> ansible/hosts
cat terraform/worker_hosts >> ansible/hosts

sed -i "s/{{SWARM_MANAGER_IP}}/$(head -n 1 terraform/manager_hosts)/g" ansible/main.yml
```

Execute um container Docker:

```shell
docker run -it --rm -v $(pwd)/ansible:/app \
  -v ~/Downloads/lacasa.pem:/ssh/lacasa.pem -w /app \
  alpine ash
```

Adicione o Ansible:

```shell
apk add ansible
```

Prepare a chave SSH:

```shell
apk add openssh-client
eval `ssh-agent -s`
ssh-add /ssh/lacasa.pem
```

Execute o playbook do Ansible:

```shell
ansible-playbook -i hosts main.yml
```

Encerre o container:

```shell
exit
```

# Destruindo a Infraestrutura na AWS (utilizando Terraform)

Execute um container Docker que contém o Terraform:

```shell
docker run -it --rm -v $(pwd)/terraform:/app -w /app --entrypoint "" \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \
  hashicorp/terraform sh
```

Inicialize o Terraform:

```shell
terraform init
```

Destrua os recursos de infraestrutura:

```shell
terraform plan -destroy -out tfdestroy
terraform apply tfdestroy
```

# REST API

### Construindo a imagem Docker

A construção da imagem Docker acontece de forma automática através de [pipelines](https://gitlab.com/la-casa-de-papel/rest-api/pipelines).

A imagem é armazenada [neste registry](https://gitlab.com/la-casa-de-papel/rest-api/container_registry).

### Fazendo o Deploy

Acesse o servidor via SSH:

```shell
ssh ubuntu@<IP>
```

Clone o projeto `rest-api`:

```shell
git clone https://gitlab.com/la-casa-de-papel/rest-api.git
cd rest-api
```

Faça o deploy da stack Docker:

```shell
docker stack deploy --compose-file docker-stack.yml ada
```

### REST Methods

#### Cadastrar uma pessoa

```txt
POST http://hackathon-sebrae.duckdns.org/pessoas
{
  "nome": "João Silva",
  "email": "joao.silva@example.com",
  "telefone": "+55-71-999999999",
  "endereco": "Rua 98",
  "empresa": "Example Services",
  "categoriaDaEmpresa": "Serviços",
  "anosDeMercado": 8
}
```

#### Obter a lista de pessoas cadastradas

```txt
GET http://hackathon-sebrae.duckdns.org/pessoas
```

#### Obter o cadastro de uma pessoa

```txt
GET http://hackathon-sebrae.duckdns.org/pessoas/{id}
```

#### Remover o cadastro de uma pessoa

```txt
DELETE http://hackathon-sebrae.duckdns.org/pessoas/{id}
```
